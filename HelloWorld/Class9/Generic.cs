namespace Class9;

public class GenericList<T> //where T: struct
{

    private T[] array = new T[10];
    private int i = 0;


    public void Add(T item)
    {
        if (i>array.Length-1)
        {
            i = 0;
            Console.WriteLine($"U added more than {array.Length} items! First item was overwritten with last item!");
        }
        array[i] = item;
        i++;
        
    }
    


    public void Print()
    {
        foreach (var item in array)
        {
            Console.WriteLine(item);
        }
    }
    
}

