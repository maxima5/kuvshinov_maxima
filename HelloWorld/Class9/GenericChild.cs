namespace Class9;

public class GenericChild:GenericList<string>
{
    public class Printer
    {
        public void Print<C>(C type) where C: class
        {
            Console.WriteLine($"type : {type.GetType()}");
        }
    }
}