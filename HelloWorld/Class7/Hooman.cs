

namespace Class7;

public class Hooman
{
    private string name;
    private int age;
    private short height;
    private byte weight;
    public eyeColor EyeColor;
    private int colors;
    private string eyecolors;
    
    
    
    public enum eyeColor
    {
        Black,
        Hazel,
        Blue,
        Green,
        Gray
    }


    public int colorsProp
    {
        get
        {
            return colors;
        }

        set
        {
            colors = value;
            if (value == (int)eyeColor.Black)
            {
                eyecolors = "Black";
            }
            if (value == (int)eyeColor.Hazel)
            {
                eyecolors = "Hazel";
            }
            if (value == (int)eyeColor.Blue)
            {
                eyecolors = "Blue";
            }
            if (value == (int)eyeColor.Green)
            {
                eyecolors = "Green";
            }
            if (value == (int)eyeColor.Gray)
            {
                eyecolors = "Gray";
            }
            
        }
    }

    public int ageProp
    {

        get
        {
            return age;
        }
        
        set
        {
            if (value >= 1 && value <= 170)
            {
                age = value;
                
            }
        }
    }

    public string nameProp
    {
        get
        {
            return name;
        }

        set
        {
            if (value != null)
                name = value;
        }
    }
    
    public short heightProp
    {
        get
        {
            return height;
        }

        set
        {
            if (value >0 && value <300)
                height = value;
        }
    }
    
    public byte weightProp
    {
        get
        {
            return weight;
        }

        set
        {
            if (value >=10 && value <=250)
                weight = value;
        }
    }
    
   
    public void Print()
    {
        Console.WriteLine("Eye color: " + eyecolors + " Age: "+ ageProp + " " +" Name: " + nameProp + " " +" Height: "  + heightProp + " " +" Weight: "  + weightProp);
        Console.ReadKey();
        
    }
    
}